﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ExtraClassLibrary.Models;

namespace ExtraClassLibrary.DataAccess
{
    public class SqlConnector : IDataConnection
    {
        public PersonModel CreateUser(PersonModel model)
        {
          using(System.Data.IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString("ExtraClass")))
            {
                var param = new DynamicParameters();
                param.Add("@UserName", model.UserName);
                param.Add("@Email", model.Email);
                param.Add("@Password", model.Password);
                param.Add("@Address", model.Address);
                param.Add("@IdRole", model.IdRole);
                param.Add("@id", 0, System.Data.DbType.Int32, direction: ParameterDirection.Output);
               
                connection.Execute("dbo.sp_user_insert", param, commandType: CommandType.StoredProcedure);
                model.Id = param.Get<int>("@id");
                return model;

            }
            
        }

        public List<RoleModel> GetAllRoles()
        {
            List<RoleModel> roles;
            
            using(IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString("ExtraClass")))
            {
                roles = connection.Query<RoleModel>("dbo.sp_get_all_roles").ToList();
            }
            return roles;
        }

        public List<PersonModel> GetAllUsers()
        {
            List<PersonModel> users = new List<PersonModel>();
            using(IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString("ExtraClass")))
            {
                users = connection.Query<PersonModel>("dbo.sp_get_all_person").ToList();
            }
            return users;
        }

        public PersonModel GetUserById(PersonModel model)
        {

            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString("ExtraClass")))
            {
                var param = new DynamicParameters();
                param.Add("@Id", model.Id);
                model = connection.QueryFirst<PersonModel>("dbo.sp_get_person",param, commandType: CommandType.StoredProcedure);
                return model;
            }
        }

        public PersonModel UpdateUser(PersonModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString("ExtraClass")))
            {
                var param = new DynamicParameters();
                param.Add("@Id", model.Id);
                param.Add("@UserName", model.UserName);
                param.Add("@Email", model.Email);
                param.Add("@Password", model.Password);
                param.Add("@Address", model.Address);
                param.Add("@IdRole", model.IdRole);
                connection.Execute("dbo.sp_update_person", param, commandType: CommandType.StoredProcedure);
                return model;
            }
        }

        public PersonModel DeleteUser(PersonModel model)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString("ExtraClass")))
            {
                var param = new DynamicParameters();
                param.Add("@Id", model.Id);
                connection.Execute("dbo.sp_delete_person", param, commandType: CommandType.StoredProcedure);
                return model;
            }
        }

    }
}
