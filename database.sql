create table person(
	id int IDENTITY(1,1) primary key,
	name varchar(255) not null,
	email varchar(255) not null unique,
	password varchar(255) not null,
	address text,
	idrole int foreign key references role(id) not null

);
select * from person;

create table role(
	id int identity(1,1) primary key,
	role varchar(100) not null
);


create table class(
	id int identity(1,1) primary key,
	class_name varchar(255) not null,
	teacher_id int foreign key references person(id) not null
)

create table class_person(
	id int identity(1,1) primary key,
	class_id int foreign key references class(id) not null,
	student_id int foreign key references person(id) not null
)

create table score(
	id int identity(1,1) primary key,
	class_id int foreign key references class(id) not null,
	student_id int foreign key references person(id) not null,
	score float not null
)

select * from role;
insert into role (role) values ('Admin');
insert into role (role) values ('Teacher');
insert into role (role) values ('Student');

create procedure sp_get_all_roles
as
	select * from role 


exec sp_get_all_roles

create procedure sp_user_insert
	@username nvarchar(255),
	@email nvarchar(255),
	@password nvarchar(255),
	@address text,
	@idrole int,
	@id int = NULL OUTPUT
	
as
begin
	set nocount on
	insert into person (username,email,password,address,idrole) values (@username,@email,@password,@address,@idrole);
	SET @id = SCOPE_IDENTITY();
end
select * from person;
exec sp_user_insert @username='Ngoc Ha',@email='ngocha080997@gmail.com',@password='123456',@address='Da Nang city',@idrole=5

--GetAllUsers
create procedure sp_get_all_person
as
	select * from person

exec sp_get_all_person

--GetUser
create procedure sp_get_person
	@id int
as 
begin
   select * from person where id=@id
end
exec sp_get_person @id=1

--UpdateUser

create procedure sp_update_person
	@id int output,
	@username nvarchar(255),
	@email nvarchar(255),
	@password varchar(255),
	@address text,
	@idrole int
as
begin
	update person set username=@username,email=@email,password=@password,address=@address,idrole=@idrole 
	where id=@id
end

exec sp_update_person @id=1,@username='ngocha123456',@email='ngocha080997@gmail.com',@password='123456',@address='Da Nang city',@idrole=1

--DeleteUser

create procedure sp_delete_person
@id int
as
begin
	delete from person where id=@id
end

exec sp_delete_person @id=9

select * from person;
select * from role;