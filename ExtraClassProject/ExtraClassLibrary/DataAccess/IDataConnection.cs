﻿using ExtraClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ExtraClassLibrary.DataAccess
{
    public interface IDataConnection
    {
        PersonModel CreateUser (PersonModel model);
        List<RoleModel> GetAllRoles();
        List<PersonModel> GetAllUsers();
        PersonModel GetUserById(PersonModel model);
        PersonModel UpdateUser(PersonModel model);
        PersonModel DeleteUser(PersonModel model);
        
    }
}
