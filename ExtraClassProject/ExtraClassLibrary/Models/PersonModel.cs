﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraClassLibrary.Models
{
    public class PersonModel
    {
        public PersonModel(){}
        public PersonModel(int id)
        {
            Id = id;
        }
        public PersonModel(string userName, string password, string email, string address,int idRole)
        {
            UserName = userName;
            Password = password;
            Email = email;
            Address = address;
            IdRole = idRole;
        }

        public PersonModel(int id,string userName,string password,string email,string address,int idRole)
        {
            Id = id;
            UserName = userName;
            Password = password;
            Email = email;
            Address = address;
            IdRole = idRole;
        }
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public int IdRole { get; set; }
  

        
    }
}
